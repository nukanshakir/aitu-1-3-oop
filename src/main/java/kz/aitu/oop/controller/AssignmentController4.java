package kz.aitu.oop.controller;

import kz.aitu.oop.entity.Student;
import kz.aitu.oop.repository.StudentFileRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileNotFoundException;
import java.util.*;

//import kz.aitu.oop.repository.StudentDBRepository;

@RestController
@RequestMapping("/api/task/4")
@AllArgsConstructor
public class AssignmentController4 {


    /**
     *
     * @param group
     * @return all student name by group name
     * @throws FileNotFoundException
     */
    @GetMapping("/group/{group}")
    public ResponseEntity<?> getStudentsByGroup(@PathVariable("group") String group) throws FileNotFoundException {

        //write your code here
        StudentFileRepository studentFileRepository = new StudentFileRepository();
        StringBuffer result = new StringBuffer();

        for(Student student: studentFileRepository.getStudents()) {
            if(group.equals(student.getGroup())) {
                result.append(student.getName()).append("<br>");
            }
        }


        return ResponseEntity.ok(result.toString());
    }

    /**
     *
     * @param group
     * @return stats by point letter (counting points): example  A-3, B-4, C-1, D-1, F-0
     * @throws FileNotFoundException
     */
    @GetMapping("/group/{group}/stats")
    public ResponseEntity<?> getGroupStats(@PathVariable("group") String group) throws FileNotFoundException {

        //write your code here
        StudentFileRepository studentFileRepository = new StudentFileRepository();

        String result = "";
        int[] arr = new int[5];

        result += group + "<br>";

        for (Student student: studentFileRepository.getStudents()) {
            if (group.equals(student.getGroup())) {
                if (student.getPoint() < 50) {
                    arr[4]++;
                } else if (student.getPoint() < 60) {
                    arr[3]++;
                } else if (student.getPoint() < 75) {
                    arr[2]++;
                } else if (student.getPoint() < 90) {
                    arr[1]++;
                } else {
                    arr[0]++;
                }
            }
        }

        result += "A-" + arr[0] + ", B-" + arr[1] + ", C-" + arr[2] + ", D-" + arr[3] + ", F-" + arr[4];

        return ResponseEntity.ok(result);

    }

    /**
     *
     * @return top 5 students name by point
     * @throws FileNotFoundException
     */
    @GetMapping("/students/top")
    public ResponseEntity<?> getTopStudents() throws FileNotFoundException {

        //write your code here

        StudentFileRepository studentFileRepository = new StudentFileRepository();
        StringBuffer result = new StringBuffer();

        String name = "";
        double top = 0;
        double total = 101;

        for (int i = 0; i < 5; i++) {
            for (Student student: studentFileRepository.getStudents()) {
                if (student.getPoint() > top && student.getPoint() < total && !student.getName().equals(name)) {
                    name = student.getName();
                    top = student.getPoint();
                }
            }
            result.append(name).append("<br>");
            total = top;
            top = 0;
        }



        /* why this one is not working?

        ArrayList arrayList = new ArrayList();

        for(Student student: studentFileRepository.getStudents()) {
                arrayList.add(student.getGroup());
        }

        Collections.sort(arrayList);

        for(int i = 0; i < 5; i++) {
            for(Student student: studentFileRepository.getStudents()) {
                if(arrayList.get(i).equals(student.getPoint())) {
                    name = student.getName();
                    result.append(name).append("<br>");
                }

            }
        }*/


        return ResponseEntity.ok(result);
    }
}
