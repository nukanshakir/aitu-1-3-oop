package kz.aitu.oop.controller;

import kz.aitu.oop.entity.Student;
//import kz.aitu.oop.examples.Point;
import kz.aitu.oop.repository.StudentFileRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileNotFoundException;
import java.util.ArrayList;

@RestController
@RequestMapping("/api/task/2")
public class AssignmentController2 {


    @GetMapping("/students")
    public ResponseEntity<?> getStudents() throws FileNotFoundException {

        //my code here
        StudentFileRepository studentFileRepository = new StudentFileRepository();

        String result = "";
        for (Student student: studentFileRepository.getStudents()) {
            result += student.getName() + "\t" + student.getAge() + "\t" + student.getPoint() + "</br>";
        }

        return ResponseEntity.ok(result);
    }

    /**
     * Method get all Students from file and calculate average name lengths
     * @return average name length of the all students
     * @throws FileNotFoundException
     */
    @GetMapping("/averageStudentsNameLength")
    public ResponseEntity<?> averageStudentsNameLength() throws FileNotFoundException {

        double average = 0;
        //my code here
        StudentFileRepository studentFileRepository = new StudentFileRepository();

        double total = 0;
        int count = 0;
        for (Student student: studentFileRepository.getStudents()) {
            total += student.getName().length();
            count++;
        }
        average = total / count;

        return ResponseEntity.ok(average);
    }

    /**
     * Method get all Students from file and calculate average count
     * @return average count of the all students
     * @throws FileNotFoundException
     */
    @GetMapping("/averageStudentsCount")
    public ResponseEntity<?> averageStudentsCount() throws FileNotFoundException {

        double count = 0;
        //change your code here
        StudentFileRepository studentFileRepository = new StudentFileRepository();

        ArrayList group = new ArrayList();

        double total = 0;
        for (Student student: studentFileRepository.getStudents()) {
            if (!group.contains(student.getGroup())) {
                group.add(student.getGroup());
            }
            total++;
        }
        count = total / group.size();

        return ResponseEntity.ok(count);
    }

    /**
     * Method get all Students from file and calculate average point
     * @return average point of the all students
     * @throws FileNotFoundException
     */
    @GetMapping("/averageStudentsPoint")
    public ResponseEntity<?> averageStudentsPoint() throws FileNotFoundException {

        double average = 0;
        //change your code here
        StudentFileRepository studentFileRepository = new StudentFileRepository();

        double total = 0;
        int count = 0;

        for(Student student: studentFileRepository.getStudents()) {
            total += student.getPoint();
            count++;
        }

        average = total / count;

        return ResponseEntity.ok(average);
    }

    /**
     * Method get all Students from file and calculate average age
     * @return average age of the all students
     * @throws FileNotFoundException
     */
    @GetMapping("/averageStudentsAge")
    public ResponseEntity<?> averageStudentsAge() throws FileNotFoundException {

        double average = 0;
        //change your code here
        StudentFileRepository studentFileRepository = new StudentFileRepository();

        double total = 0;
        int count = 0;

        for(Student student: studentFileRepository.getStudents()) {
            total += student.getAge();
            count++;
        }

        average = total / count;

        return ResponseEntity.ok(average);
    }

    /**
     * Method get all Students from file and calculate highest point
     * @return highest point of the all students
     * @throws FileNotFoundException
     */
    @GetMapping("/highestStudentsPoint")
    public ResponseEntity<?> highestStudentsPoint() throws FileNotFoundException {

        double maxPoint = 0;
        //change your code here
        StudentFileRepository studentFileRepository = new StudentFileRepository();

        double count = 0;

        for(Student student: studentFileRepository.getStudents()) {
            count = student.getPoint();
            if (maxPoint < count) {
                maxPoint = count;
            }
        }

        return ResponseEntity.ok(maxPoint);
    }

    /**
     * Method get all Students from file and calculate highest age
     * @return highest age of the all students
     * @throws FileNotFoundException
     */
    @GetMapping("/highestStudentsAge")
    public ResponseEntity<?> highestStudentsAge() throws FileNotFoundException {

        double maxAge = 0;
        //change your code here
        StudentFileRepository studentFileRepository = new StudentFileRepository();

        double count = 0;

        for(Student student: studentFileRepository.getStudents()) {
            count = student.getAge();
            if (maxAge < count) {
                maxAge = count;
            }
        }
        return ResponseEntity.ok(maxAge);
    }

    /**
     * Method get all Students from file and calculate highest group average point
     * @return highest group average point of the all students
     * @throws FileNotFoundException
     */
    @GetMapping("/highestGroupAveragePoint")
    public ResponseEntity<?> highestGroupAveragePoint() throws FileNotFoundException {

        double averageGroupPoint = 0;
        //change your code here
        StudentFileRepository studentFileRepository = new StudentFileRepository();

        String result = "";
        int total = 1;
        double flag = 0;

        for (Student student: studentFileRepository.getStudents()) {
            if (student.getGroup().equals(result)) {
                flag += student.getPoint();
                total++;
            }
            else {
                result = student.getGroup();
                if (averageGroupPoint < flag / total) {
                    averageGroupPoint = flag / total;
                }
                total = 1;
                flag = student.getPoint();
            }
        }

        return ResponseEntity.ok(averageGroupPoint);
    }

    /**
     * Method get all Students from file and calculate highest group average age
     * @return highest group average age of the all students
     * @throws FileNotFoundException
     */
    @GetMapping("/highestGroupAverageAge")
    public ResponseEntity<?> highestGroupAverageAge() throws FileNotFoundException {

        double averageGroupAge = 0;
        //change your code here
        StudentFileRepository studentFileRepository = new StudentFileRepository();

        String result = "";
        int total = 1;
        double flag = 0;

        for (Student student: studentFileRepository.getStudents()) {
            if (student.getGroup().equals(result)) {
                flag += student.getAge();
                total++;
            }
            else {
                result = student.getGroup();
                if (averageGroupAge < flag / total) {
                    averageGroupAge = flag / total;
                }
                total = 1;
                flag = student.getAge();
            }
        }
        return ResponseEntity.ok(averageGroupAge);
    }


    @GetMapping("/all")
    public ResponseEntity<?> allData() throws FileNotFoundException {

        String result = "";
        result += "averageStudentsNameLength: " + averageStudentsNameLength().getBody() + "</br>";
        result += "averageStudentsCount: " + averageStudentsCount().getBody() + "</br>";
        result += "averageStudentsPoint: " + averageStudentsPoint().getBody() + "</br>";
        result += "averageStudentsAge: " + averageStudentsAge().getBody() + "</br>";
        result += "highestStudentsPoint: " + highestStudentsPoint().getBody() + "</br>";
        result += "highestStudentsAge: " + highestStudentsAge().getBody() + "</br>";
        result += "highestGroupAveragePoint: " + highestGroupAveragePoint().getBody() + "</br>";
        result += "highestGroupAverageAge: " + highestGroupAverageAge().getBody() + "</br>";

        return ResponseEntity.ok(result);
    }
}

