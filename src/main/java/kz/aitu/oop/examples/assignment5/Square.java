package kz.aitu.oop.examples.assignment5;

public class Square extends Rectangle {

    public Square () {
        super(1.0, 1.0);
    }

    public Square (double side) {
        super(side, side);
    }

    public Square (double side, String color, boolean filled) {
        super(side, side, color, filled);
    }

    public double getSide () {
        return super.getLength();
    }

    public void setSide (double side) {
        super.setLength(side);
    }

    @Override
    public void setLength(double side) {
        super.setLength(side);
        super.setWidth(side);
    }

    @Override
    public void setWidth(double side) {
        super.setLength(side);
        super.setWidth(side);
    }

    @Override
    public double getArea() {
        return super.getArea();
    }

    @Override
    public double getPerimeter() {
        return super.getPerimeter();
    }

    @Override
    public String toStrings(){
        return "A square with a side " + getSide() + " which is a subclass of " + super.toStrings();
    }
}
