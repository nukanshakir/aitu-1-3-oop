package kz.aitu.oop.examples.assignment3;

import java.util.Scanner;

public class Main {

    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);

        int size = sc.nextInt();
        int[] arr = new int[size];

        for(int i = 0; i < size; i++) {
            arr[i] = sc.nextInt();
        }

        NewSpecialString str = new NewSpecialString(arr);
        System.out.println(str.valueAt(3));
    }

}


