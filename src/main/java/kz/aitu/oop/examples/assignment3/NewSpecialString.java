package kz.aitu.oop.examples.assignment3;

import java.util.LinkedHashSet;

public class NewSpecialString {
    private Integer[] arr;

    //task 5//

    public NewSpecialString(int[] values) {
        LinkedHashSet<Integer> copy = new LinkedHashSet<Integer>();

        for(int i = 0; i < values.length; i++) {
            copy.add(values[i]);
        }

        arr = copy.toArray(new Integer[]{});
    }

    public int length() {
        return arr.length;
    }

    public String valueAt(int position) {
        if(position >= 0 && position < arr.length) {
            return "value is " + arr[position];
        } else {
            return "-1";
        }
    }

    public boolean contains(int value) {

        for(int i = 0; i < arr.length; i++) {
            if(arr[i] == value) {
                return true;
            }
        }

        return false;
    }

    public int count(int value) {
        int count = 0;

        for(int i = 0; i < arr.length; i++) {
            if (arr[i] == value) {
                count++;
            }
        }
        return count;
    }

    public void printStored() {
        for(int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
    }
}
