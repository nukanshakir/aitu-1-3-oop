package kz.aitu.oop.examples.assignment3;


import java.io.*;
import java.util.Scanner;

public class MyString {

    private int[] arr;
    private String fileName;

    public MyString(int[] values) {
        arr = values;
    }

    public int[] getArr() {
        return arr;
    }

    //task 1//
    public int length() {
        return arr.length;
    }

    public int valueAt(int position) {
        if(position >= 0 && position < arr.length) {
            return arr[position];
        } else {
            return -1;
        }
    }

    public boolean contains(int value) {

        for(int i = 0; i < arr.length; i++) {
            if(arr[i] == value) {
                return true;
            }
        }

        return false;
    }

    public int count(int value) {
        int count = 0;

        for(int i = 0; i < arr.length; i++) {
            if (arr[i] == value) {
                count++;
            }
        }
        return count;
    }

    public void printStored() {
        for(int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
    }


    //task 2, 3, 4//
    public void InputFile(MyString values) {
        Scanner sc = new Scanner(System.in);
        String b = sc.next();


        try {
            fileName = "/Users/Damir/Desktop/" + b + ".txt";
            new FileOutputStream(fileName);
        } catch (IOException e) {}

        try(FileWriter fileWriter = new FileWriter(fileName, true);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            PrintWriter out = new PrintWriter(bufferedWriter))
        {
            int[] check = values.getArr();

            for (int i = 0; i < check.length-1; i++ ) {
                out.print(check[i] + ", ");
            }
            out.println(check[check.length-1]);
        } catch (IOException e) {}



    }

    public Object ReadFile(MyString values) throws FileNotFoundException {
        File file = new File(fileName);
        Scanner sc = new Scanner(file);

        int[] check = new int[length()];

        int i = 0;
        while (sc.hasNextInt()) {
            check[i] = sc.nextInt();
            i++;
        }

        MyString alter = new MyString(check);
        return alter;
    }



}
