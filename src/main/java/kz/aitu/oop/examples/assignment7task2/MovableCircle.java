package kz.aitu.oop.examples.assignment7task2;


public class MovableCircle implements Movable {

    private int radius;
    private MovablePoint center;

    public MovableCircle(int x, int y, int xSpeed, int ySpeed, int radius) {

        center.setX(x);
        center.setY(y);
        center.setxSpeed(xSpeed);
        center.setySpeed(ySpeed);
        this.radius = radius;
    }

    @Override
    public void moveLeft() {
        center.setX(center.getX()-1);
    }

    @Override
    public void moveRight() {
        center.setX(center.getX()+1);
    }

    @Override
    public void moveUp() {
        center.setY(center.getY()+1);
    }

    @Override
    public void modeDown() {
        center.setY(center.getY()-1);
    }

    public String toStrings () {
        return "Circle with the radius" + this.radius + " moves " + center.getX() + " to the right, " + center.getY() + " to the left. Speed is " + center.getxSpeed() + " to the right, " + center.getySpeed() + " to  the left.";
    }
}

