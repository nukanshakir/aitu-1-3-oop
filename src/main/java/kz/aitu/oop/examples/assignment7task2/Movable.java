package kz.aitu.oop.examples.assignment7task2;

public interface Movable {
    void moveUp();
    void modeDown();
    void moveLeft();
    void moveRight();
}

