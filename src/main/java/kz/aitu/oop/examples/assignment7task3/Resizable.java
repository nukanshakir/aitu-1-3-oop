package kz.aitu.oop.examples.assignment7task3;

public interface Resizable {
    void resize(int percent);
}
