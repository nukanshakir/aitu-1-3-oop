package kz.aitu.oop.examples.assignment7task3;

abstract public class Circle implements GeometricObject {

    protected double radius;

    public Circle () {
        this.radius = 1.0;
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public double getPerimeter() {
        return (this.radius * 2) * Math.PI;
    }

    @Override
    public double getArea() {
        return Math.pow(this.radius, 2) * Math.PI;
    }

    public String toStrings() {
        return "Circle with radius " + this.radius + " has area and perimeter of " + getArea() + " and " + getPerimeter() + ".";
    }
}
