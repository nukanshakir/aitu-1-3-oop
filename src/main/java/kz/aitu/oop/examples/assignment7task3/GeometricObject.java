package kz.aitu.oop.examples.assignment7task3;

public interface GeometricObject {
    double getPerimeter();
    double getArea();
}
