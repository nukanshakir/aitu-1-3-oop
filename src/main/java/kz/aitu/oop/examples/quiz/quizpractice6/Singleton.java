package kz.aitu.oop.examples.quiz.quizpractice6;

public class Singleton {

    private static Singleton instance;

    public String str;

    private Singleton() {

    }

    public static synchronized Singleton getSingleInstance() {

        if(instance == null) {
            instance = new Singleton();
        }

        return instance;
    }


}
