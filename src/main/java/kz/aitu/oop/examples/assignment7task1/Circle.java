package kz.aitu.oop.examples.assignment7task1;

public class Circle extends Shape {

    protected double radius;

    public Circle () { this.radius = 1.0; }

    public Circle (double radius) { this.radius = radius; }

    public Circle (double radius, String color, boolean filled) {
        super(color, filled);
        this.radius = radius;
    }

    public double getRadius () { return this.radius; }

    public void setRadius (double radius) { this.radius = radius; }

    public double getArea () { return Math.pow(this.radius, 2) * Math.PI; }

    public double getPerimeter () {
        return (this.radius * 2) * Math.PI;
    }

    @Override
    public String toStrings() {
        return "A circle with a radius " + this.radius + " which is a subclass of " + super.toStrings();
    }
}
