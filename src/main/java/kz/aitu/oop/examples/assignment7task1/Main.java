package kz.aitu.oop.examples.assignment7task1;

public class Main {

    public static void main(String[] args) {

        Shape s1 = new Circle(5.5, "red", false); // Upcast Circle to Shape

        System.out.println(s1); // the address through packages and memory

        // Class Circle uses abstract method getArea() from class Shape
        System.out.println(s1.getArea());

        // Class Circle uses abstract method getPerimeter() from class Shape
        System.out.println(s1.getPerimeter());

        // Using methods that exist in the class Shape
        System.out.println(s1.getColor());
        System.out.println(s1.isFilled());

        /*Cannot resolve method 'getRadius' in 'Shape'
        System.out.println(s1.getRadius());
        */

        System.out.println("------------------------------");

        Circle c1 = (Circle)s1; // new object with same configurations

        System.out.println(c1); // the address through packages and memory

        // Using methods that exist in the class Shape
        System.out.println(c1.getArea());
        System.out.println(c1.getPerimeter());
        System.out.println(c1.getColor());
        System.out.println(c1.isFilled());

        System.out.println(c1.getRadius()); // proof that it has the same configurations

        System.out.println("------------------------------");


        /*'Shape' is abstract; cannot be instantiated [Abstract class in not regular class, it only helps to make it]
        Shape s2 = new Shape();
        */

        Shape s3 = new Rectangle(1.0, 2.0, "red", false); // Creating new object

        System.out.println(s3); // the address through packages and memory

        // Using methods that exist in the class Shape
        System.out.println(s3.getArea());
        System.out.println(s3.getPerimeter());
        System.out.println(s3.getColor());

        /*Cannot resolve method 'getLength' in 'Shape' [There is no such a method in the class Shape]
        System.out.println(s3.getLength());
        */

        System.out.println("------------------------------");

        Rectangle r1 = (Rectangle)s3; // new object with same configurations

        System.out.println(r1); // the address through packages and memory

        // Using methods that exist in the class Shape
        System.out.println(r1.getArea());
        System.out.println(r1.getColor());
        System.out.println(r1.getLength());

        System.out.println("------------------------------");

        Shape s4 = new Square(6.6); // Creating new object

        System.out.println(s4); // the address through packages and memory

        // Using methods that exist in the class Shape
        System.out.println(s4.getArea());
        System.out.println(s4.getColor());

        /*Cannot resolve method 'getSide' in 'Shape' [There is no such a method in the class Shape]
        System.out.println(s4.getSide());
        */

        System.out.println("------------------------------");

        Rectangle r2 = (Rectangle)s4; // new object with same configurations

        System.out.println(r2); // the address through packages and memory

        // Using methods that exist in the class Shape
        System.out.println(r2.getArea());
        System.out.println(r2.getColor());

        /*Cannot resolve method 'getSide' in 'Rectangle' [There is no such a method in the class Rectangle]
        System.out.println(r2.getSide());
        */

        // Since the method does exist in the class Rectangle we can use it
        System.out.println(r2.getLength());

        System.out.println("------------------------------");

        Square sq1 = (Square)r2; // new object with same configurations

        System.out.println(sq1); // the address through packages and memory

        // Using methods that exist in the class Shape
        System.out.println(sq1.getArea());
        System.out.println(sq1.getColor());

        // Using methods that exist in the classes Square and Rectangle
        System.out.println(sq1.getSide());
        System.out.println(sq1.getLength());

        /*What is the usage of the abstract method and abstract class?
        Abstract class is not regular class it only helps to make them.
        Classes that were made from abstract class are called subclasses.
        Subclasses can use abstract methods if they exist.
         */
    }

}
